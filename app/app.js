(function(){
  'use strict';
  
  angular
    .module('App', [
      'ui.router'
    ])
    .config(AppConfig)
    .run(App);

  AppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
  App.$inject = ['$rootScope', '$http', 'api'];
  
  function AppConfig($stateProvider, $urlRouterProvider) {
    var overviewState = {
      url: '/',
      views:{
        '': { templateUrl: 'app/home/overview.view.html', controller: 'OverviewController' },
        'navigation': { templateUrl: 'app/home/navigation.view.html', controller: 'NavigationController' }
      }
    };
    
    var expensesState = {
      url: '/expenses',
      views:{
        '': { templateUrl: 'app/home/expenses.view.html', controller: 'ExpensesController' },
        'navigation': { templateUrl: 'app/home/navigation.view.html', controller: 'NavigationController' }
      }
    };
    
    var incomesState = {
      url: '/incomes',
      views:{
        '': { templateUrl: 'app/home/incomes.view.html', controller: 'IncomesController' },
        'navigation': { templateUrl: 'app/home/navigation.view.html', controller: 'NavigationController' }
      }
    };

    $stateProvider
    .state('overview', overviewState)
    .state('expenses', expensesState)
    .state('incomes', incomesState);

    $urlRouterProvider.otherwise('/');
  }

  function App($rootScope, $http) {
    var url = 'http://toshl-killer.herokuapp.com/api/v1/balance_changes';
    
    $rootScope.date = new Date();
    
    $rootScope.models = [];
    
    $rootScope.getAll = function () {
      
      var filterString = new moment($rootScope.date).format('YYYY-MM');
      
      $http({
          method: 'GET',
          url: url + '?filter[period]=' + filterString
      }).success(function(data, status) {
        
        $rootScope.totalIncomes = 0;
        $rootScope.incomes = data.data
            .filter(function (obj) {
                return obj.attributes.change_type == 'income';
            })
            .map(function(obj){
                var model = {
                    id: obj.id,
                    date: obj.attributes.entry_date,
                    value: obj.attributes.value
                };
                $rootScope.totalIncomes = $rootScope.totalIncomes + model.value;
                return model;
            });
              
        $rootScope.totalExpenses = 0;
        $rootScope.expenses = $rootScope.models = data.data
          .filter(function (obj) {
              return obj.attributes.change_type == 'expense';
          })
          .map(function(obj){
              var model = {
                id: obj.id,
                date: obj.attributes.entry_date,
                value: obj.attributes.value
              };
              $rootScope.totalExpenses = $rootScope.totalExpenses + model.value;
              return model;
          });
      });
    };
    /**
     * App logic goes here
     */
  }
})();