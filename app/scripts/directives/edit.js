'use strict';
  
 angular.module('App')
  .directive('edit', function ($rootScope, api) {
      return{
          restric: 'E',
          templateUrl: 'app/home/edit.template.html',
          controller: function () {
              this.save = function (editModel, form) {
                  if(!editModel.id){
                      var insertModel = {
                          data:{
                              attributes:{
                                  value: editModel.value,
                                  entry_date: editModel.date,
                                  change_type: editModel.type
                              }
                          }
                      };
                      api.insertBalanceChange(insertModel);
                  }else{
                      var updateModel = {
                          data:{
                              attributes:{
                                  value: editModel.value,
                                  entry_date: editModel.date,
                                  change_type: editModel.type
                              }
                          }
                      };
                      api.updateBalanceChange(editModel.id, updateModel);
                  }
                  
                  if (form) {
                    form.$setPristine();
                 }
                $rootScope.editModel = {
                    type: editModel.type
                };
                $rootScope.isEdit = false;
              }
          },
          controllerAs: 'editCtrl'
      };
  });