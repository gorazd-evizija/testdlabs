'use strict';
  
 angular.module('App')
  .directive('navigation', function ($rootScope) {
      return{
          restric: 'E',
          templateUrl: 'app/home/navigation.template.html',
          controller: function () {
              this.previous = function () {
                  $rootScope.date = new moment($rootScope.date).add(-1, 'months').toDate();
                  $rootScope.getAll();
              };
              
              this.next = function () {
                  $rootScope.date = new moment($rootScope.date).add(1, 'months').toDate();
                  $rootScope.getAll();
              };
          },
          controllerAs: 'nav'
      };
  });