angular.module('App')
  .factory('api', function ($rootScope, $http) {
      var url = 'http://toshl-killer.herokuapp.com/api/v1/balance_changes';
      return {
          deleteBalanceChange: function (id) {
            $http.delete(url + '/' + id).then(function (status) {
                $rootScope.getAll();
                });
            },
            insertBalanceChange: function (balanceChange) {
                $http.post(url, balanceChange).then(function (results) {
                    $rootScope.getAll();
                });
            },
            updateBalanceChange: function (id, balanceChange) {
                $http.put(url + '/' + id, balanceChange).then(function (status) {
                    $rootScope.getAll();
                });
            }
      };
  });