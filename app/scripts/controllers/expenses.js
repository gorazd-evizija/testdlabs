'use strict';

angular.module('App')
  .controller('ExpensesController', function ($rootScope, api) {
    $rootScope.getAll();
    $rootScope.editModel = {
      type: 'expense'
    };
    $rootScope.isEdit = false;
    
    this.addClick = function () {
      $rootScope.isEdit = true;
    };
    
    this.edit = function (editModel) {
      $rootScope.isEdit = true;
      $rootScope.editModel = editModel;
      $rootScope.editModel.type = 'expense';
    };
    
    this.remove = function (id) {
      api.deleteBalanceChange(id);
    };
  });