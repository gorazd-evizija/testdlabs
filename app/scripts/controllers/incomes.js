'use strict';
  
 angular.module('App').controller('IncomesController', function($rootScope, api) {
    $rootScope.getAll();
    $rootScope.editModel = {
      type: 'income'
    };
    $rootScope.isEdit = false;
    
    this.addClick = function () {
      $rootScope.isEdit = true;
    };
    
    this.edit = function (editModel) {
      $rootScope.isEdit = true;
      $rootScope.editModel = editModel;
      $rootScope.editModel.type = 'income';
    };
    
    this.remove = function (id) {
      api.deleteBalanceChange(id);
    };
});